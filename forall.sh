#!/bin/bash 

set -o pipefail

export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

if [[ "$1" == "-u" ]] ; then
    shift
    uniqueres=true
else
    uniqueres=false
fi
if [[ "$1" == "-q" ]] ; then
    shift
    quiet=true
else
    quiet=false
fi
if [[ "$1" == "-a" ]] ; then
    shift
    all=true
else
    all=false
fi

if [[ $# -ne 1 ]] ; then
    echo "Missing argument"
    exit
fi

pbcopy='xsel --clipboard --input'

baseurl="https://de.wikipedia.org/w/api.php?action=query&format=json&prop=revisions&rvprop=content&rvlimit=1&titles=Wikipedia%3AUmfragen%2FTechnische+W%C3%BCnsche+2017%2F"

rubriken=("Lesen" "Suchen" "Bearbeiten" "Wartung" \
            "Beobachten_%26_Benachrichtigen" "Soziales" \
            "Schwesterprojekte" "Mediendateien" \
            "Projekte_ehrenamtlicher_Entwickler" "Sonstiges")
if $all; then
    rubriken=("${rubriken[@]}" "Bereits_umgesetzte_Wünsche" "Wünsche_außerhalb_des_Projektrahmens")
fi

if $uniqueres; then
	for r in "${rubriken[@]}"; do
	    curl -s "${baseurl}${r}" 
	done | jq -r '.query .pages | .[] .revisions | .[0] ."*"' | awk -f "$1" 
else
	for r in "${rubriken[@]}"; do
	    $quiet || echo "${r}"
	    curl -s "${baseurl}${r}" > foo
	    cat foo | jq -r '.query .pages | .[] .revisions | .[0] ."*"' | awk -f "$1" && rm foo
	done
fi
