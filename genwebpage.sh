#!/bin/bash

set -o pipefail

export TZ="Europe/Berlin"

cd /data/project/twl17/script
./forall.sh -u -q voteresults.html.awk | sort -nr > ../public_html/index.txt
test $? -ne 0 && exit

cat ../public_html/head > ../public_html/index.html
cat ../public_html/index.txt >> ../public_html/index.html
echo -e "\n</table>\n<br/><hr><br/>\nStand: " >>  ../public_html/index.html
date >>  ../public_html/index.html
echo -e "\n</body></html>" >>  ../public_html/index.html
