BEGIN {
	IGNORECASE = 1
	count=0
	votingsection=0
	allvotes=0
	print("") > "badsig"
	print("") > "problemcases"
}

/^\|/ || /^}}/ {
	votingsection=0
}

/^==.*==$/ { 
	if ( count )
		perwish()
	titel=substr($0,4,length($0)-6)
	count++
}

/Vorschlagende Person=.*/ {
	match($0, /(Spezial:Beiträge\/|User:|Benutzer:|Benutzerin:)([^|]*)\|/, res)
	einreicher=res[2]
}

/^\|Unterstützung=/ {
	votingsection=1
	votes=0
}

function pervote() {
	votes++
	allvotes++
	match($0, /(Spezial:Beiträge\/|User:|Benutzer:|Benutzerin:|BD:|Benutzer Diskussion:|Unsigned\||Unsigniert\||unsigned\||unsigniert\|)([^|]*)[|\]}]/, res)
	print(res[2]) > "voters"
	if(res[2] == "") {
		print($0) > "badsig"
	}
}

(/{{pro}}/ || /'''dafür'''/) &&
votingsection == 1 {
	pervote()
}

(/Benutzer/ || /User/ || /BD:/) &&
!/{{[ck]ontra}}/       &&
!/{{neutral}}/         &&
!/{{enthaltung}}/         &&
!/{{abwartend}}/       &&
!/{{info/             &&
!/{{pro}}/             &&
!/'''dafür'''/         &&
!/^#[:*]/              &&
votingsection == 1 {
	pervote()
	print($0) > "problemcases"
}
